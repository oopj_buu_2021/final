package com.boonanan.midterm;

import java.util.Scanner;
import javax.swing.JOptionPane;

public class AppFrame extends javax.swing.JFrame {

    public AppFrame() {
        initComponents();
        setClear();
        setDisable();
        btnStart.setEnabled(true);
        btnExit.setEnabled(false);
        setEnvironment();

    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnStart = new javax.swing.JButton();
        btnExit = new javax.swing.JButton();
        btnRun = new javax.swing.JButton();
        lblHeader1 = new javax.swing.JLabel();
        lblHeader2 = new javax.swing.JLabel();
        btnRule = new javax.swing.JButton();
        lblTurn = new javax.swing.JLabel();
        lblRound = new javax.swing.JLabel();
        lblCurrentRound = new javax.swing.JLabel();
        lblCurrentTurn = new javax.swing.JLabel();
        cmbDirection = new javax.swing.JComboBox<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        ShowPanel = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));

        btnStart.setText("Start");
        btnStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStartActionPerformed(evt);
            }
        });

        btnExit.setText("Exit");
        btnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });

        btnRun.setText("Run");
        btnRun.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRunActionPerformed(evt);
            }
        });

        lblHeader1.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        lblHeader1.setForeground(new java.awt.Color(255, 102, 102));
        lblHeader1.setText("Chase");

        lblHeader2.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        lblHeader2.setForeground(new java.awt.Color(255, 0, 51));
        lblHeader2.setText("The Flirting");

        btnRule.setText("Rule");
        btnRule.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRuleActionPerformed(evt);
            }
        });

        lblTurn.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        lblTurn.setText("Turn :");

        lblRound.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        lblRound.setText("Round :");

        lblCurrentRound.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        lblCurrentRound.setText("1");

        lblCurrentTurn.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        lblCurrentTurn.setText("You");

        cmbDirection.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Right", "Left", "Up", "Down" }));
        cmbDirection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbDirectionActionPerformed(evt);
            }
        });

        ShowPanel.setEditable(false);
        ShowPanel.setColumns(20);
        ShowPanel.setRows(5);
        jScrollPane2.setViewportView(ShowPanel);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(19, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblHeader1)
                    .addComponent(lblHeader2)
                    .addComponent(btnStart, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRule, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnExit, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 311, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblRound)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblCurrentRound, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblTurn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblCurrentTurn, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(cmbDirection, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRun))
                .addContainerGap(19, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(19, 19, 19)
                                .addComponent(lblHeader1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblHeader2)
                                .addGap(245, 245, 245)
                                .addComponent(btnRule)
                                .addGap(18, 18, 18)
                                .addComponent(btnStart))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(lblRound)
                                    .addComponent(lblCurrentRound))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(lblTurn)
                                    .addComponent(lblCurrentTurn))
                                .addGap(74, 74, 74)
                                .addComponent(cmbDirection, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnRun)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnExit))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 406, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(12, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExitActionPerformed
        int result = JOptionPane.showConfirmDialog(this, "Are you sure?");

        switch (result) {
            case 0:
                System.exit(0);
                break;
            default:
                break;
        }
    }//GEN-LAST:event_btnExitActionPerformed

    private void btnRunActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRunActionPerformed
        String direction = (String) cmbDirection.getSelectedItem();
        String command = lblCurrentTurn.getText() + direction;
        Process(command);
        //showProcess();
        nextRound();
    }//GEN-LAST:event_btnRunActionPerformed

    private void btnRuleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRuleActionPerformed
        JOptionPane.showMessageDialog(this,
                "- If you can chase one of the two, you win.\n"
                + "- if colliding or hitting a building, it will be Game Over.\n"
                + "- The man walks 2 steps at a time, and ex-girl takes 1 step at a time.\n"
                + "You can walk 3 steps at a time.\n"
                + "- All three must complete their own missions within 30 rounds.\n"
                + "- If no one can catch each other, it will be a draw immediately.");
    }//GEN-LAST:event_btnRuleActionPerformed

    private void cmbDirectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbDirectionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbDirectionActionPerformed

    private void btnStartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStartActionPerformed
        setEnable();
        btnStart.setEnabled(false);
        btnExit.setEnabled(true);
        setClear();

    }//GEN-LAST:event_btnStartActionPerformed

    private void setDisable() {
        ShowPanel.setEnabled(false);
        btnRule.setEnabled(false);
        btnRun.setEnabled(false);
        cmbDirection.setEnabled(false);
    }

    private void setEnable() {
        ShowPanel.setEnabled(true);
        btnRule.setEnabled(true);
        btnRun.setEnabled(true);
        cmbDirection.setEnabled(true);
    }

    private void setClear() {
        ShowPanel.setText("");
        cmbDirection.setSelectedIndex(0);
        lblCurrentTurn.setText("You");
        lblCurrentRound.setText("1");

    }

    private void nextRound() {
        round++;
        if (round == 31) {
            JOptionPane.showMessageDialog(this, "Draw!!, No one can catch each other.");
            round = 1;
            setClear();
            setDisable();
            btnStart.setEnabled(true);
            btnExit.setEnabled(false);
        }
        lblCurrentRound.setText(Integer.toString(round));
        nextTurn();
    }

    private void nextTurn() {
        if (round % 3 == 0) {
            lblCurrentTurn.setText("Ex-girl");
        } else if (round % 3 == 1) {
            lblCurrentTurn.setText("You");
        } else {
            lblCurrentTurn.setText("Boyfriend");
        }
    }

    private void setEnvironment() {
        map.add(Male);
        map.add(FeMale1);
        map.add(FeMale2);
        map.add(building1);
        map.add(building2);
        map.add(building3);
        map.add(building4);
        map.add(building5);
        map.add(building6);
        map.add(building7);
        map.add(building8);
        map.add(building9);
        map.add(building10);
        map.add(building11);
        map.add(building12);
    }

    public static void Process(String direction) {
        switch (direction) {
            case "BoyFriendUp":
                Male.up(2);
                break;
            case "BoyFriendLeft":
                Male.left(2);
                break;
            case "BoyFriendDown":
                Male.down(2);
                break;
            case "BoyFriendRight":
                Male.right(2);
                break;
            case "Ex-girlUp":
                FeMale1.up(1);
                break;
            case "Ex-girlLeft":
                FeMale1.left(1);
                break;
            case "Ex-girlDown":
                FeMale1.down(1);
                break;
            case "Ex-girlRight":
                FeMale1.right(1);
                break;
            case "YouUp":
                FeMale2.up(3);
                break;
            case "YouLeft":
                FeMale2.left(3);
                break;
            case "YouDown":
                FeMale2.down(3);
                break;
            case "YouRight":
                FeMale2.right(3);
                break;
            default:
                break;
        }
    }

    private void showProcess() {

    }

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AppFrame().setVisible(true);
            }
        });
    }

    static int n = 1;
    static int round = 1;
    static String turn = "You";
    static Scanner sc = new Scanner(System.in);
    static Map map = new Map(20, 20);
    static People Male = new People(map, "M", 0, 19);
    static People FeMale1 = new People(map, "P", 19, 0);
    static People FeMale2 = new People(map, "G", 8, 8);
    static Building building1 = new Building(map, 5, 7);
    static Building building2 = new Building(map, 7, 8);
    static Building building3 = new Building(map, 4, 10);
    static Building building4 = new Building(map, 7, 7);
    static Building building5 = new Building(map, 6, 2);
    static Building building6 = new Building(map, 7, 12);
    static Building building7 = new Building(map, 9, 15);
    static Building building8 = new Building(map, 12, 15);
    static Building building9 = new Building(map, 18, 6);
    static Building building10 = new Building(map, 16, 8);
    static Building building11 = new Building(map, 13, 14);
    static Building building12 = new Building(map, 13, 17);
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea ShowPanel;
    private javax.swing.JButton btnExit;
    private javax.swing.JButton btnRule;
    private javax.swing.JButton btnRun;
    private javax.swing.JButton btnStart;
    private javax.swing.JComboBox<String> cmbDirection;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblCurrentRound;
    private javax.swing.JLabel lblCurrentTurn;
    private javax.swing.JLabel lblHeader1;
    private javax.swing.JLabel lblHeader2;
    private javax.swing.JLabel lblRound;
    private javax.swing.JLabel lblTurn;
    // End of variables declaration//GEN-END:variables

}
